document.addEventListener("DOMContentLoaded", function (event) {

    var button = document.getElementById("valider");
    var text = document.getElementById("nom");
    var resultElement = document.getElementById("result");

    button.addEventListener("click", function (e) {

        e.preventDefault();

        var value = text.value;

        while (resultElement.firstChild) {
            resultElement.removeChild(resultElement.firstChild);
        }

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "http://localhost:1234/api?action=opensearch&format=json&formatversion=2&search=" + value + "&namespace=0&limit=10&suggest=true");

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var response = xmlhttp.responseText;
                var myJson = JSON.parse(response);

                var ulElement = document.createElement("ul");
                resultElement.appendChild(ulElement);

                var liElement = document.createElement("li");

                for (var i = 0 ; i < myJson[3].length ; i++){
                    liElement.textContent = myJson[3][i];
                    var cloneElement = liElement.cloneNode(true);
                    ulElement.appendChild(cloneElement);
                }
            }
        };

        xmlhttp.send();

    });
});